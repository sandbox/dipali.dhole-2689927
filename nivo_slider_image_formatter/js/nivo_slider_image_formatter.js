(function ($) {
  Drupal.behaviors.nivo_slider_image_formatter = {
    attach: function (context, settings) {
      var nf_setting = settings.nivo_slider_image_formatter;
      $(window).load(function () {
        for (var field_name in nf_setting) {
          $("#" + field_name).nivoSlider(nf_setting[field_name]);
        }
      });
    }
  };
}(jQuery));
