
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainer: Dipali Dhole <dipali.dhole27@gmail.com>

Nivo Slider Image Formatter is an image field formatter that transform any image field 
to awesome <a href="http://nivo.dev7studios.com/">Nivo Slider</a> image gallery.

For user:
- Support image style.
- Support thumbnail.
- Support most settings of Nivo Slider, except advanced triggers callback
settings.

INSTALLATION
------------
1. Create directory libraries in Drupal Install

2. Download <a href="http://nivo.dev7studios.com/">Nivo slider</a> and extract 
   to libraries/nivo-slider.
   Ensure this path exists:
   libraries/nivo-slider/jquery.nivo.slider.pack.js

3. Download and enable nivo_slider_image_formatter module.

4. Add field image if not existed to your content type admin/structure/types
   Select more than one value for your image field.

5. At Content's Manage Display tab, field Image, select Nivo Slider Formatter as format.
   Tweak setting as you like. Update -> Save.

6. Create content, upload some image and see Nivo Slider in action.
