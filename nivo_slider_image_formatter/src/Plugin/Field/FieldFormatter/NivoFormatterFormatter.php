<?php

/**
 * @file
 * Contains \Drupal\nivo_slider_image_formatter\Plugin\Field\FieldFormatter\GalleryFormatterFormatter.
 */

namespace Drupal\nivo_slider_image_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\image\Entity\ImageStyle;

/**
 * Plugin for nivo_slider_image_formatter.
 *
 * @FieldFormatter(
 *   id = "nivo_slider_image_formatter",
 *   label = @Translation("Nivo Slider Formatter "),
 *   field_types = {
 *     "image",
 *   }
 * )
 */
class NivoFormatterFormatter extends ImageFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'nivo_formatter_theme' => 'default',
      'nivo_formatter_image_style' => 'large',
      'nivo_slider_thumbnail' => array(
        'controlNavThumbs' => FALSE,
        'controlNavThumbsFromRel' => FALSE,
        'thumbnail_style' => 'thumbnail',
      ),
      'nivo_formatter_nivo_slider' => array(
        'effect' => array('random' => 'random'),
        'slices' => 15,
        'boxCols' => 8,
        'boxRows' => 4,
        'animSpeed' => 500,
        'pauseTime' => 3000,
        'startSlide' => 0,
        'directionNav' => TRUE,
        'directionNavHide' => TRUE,
        'controlNav' => TRUE,
        'keyboardNav' => TRUE,
        'pauseOnHover' => TRUE,
        'manualAdvance' => FALSE,
        'captionOpacity' => 0.8,
        'prevText' => 'Prev',
        'nextText' => 'Next'
      )
        ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $image_styles = image_style_options(FALSE);
    $options = [t('None')];
    $settings = $this->getSetting('settings');
	
    $themes = nivo_slider_image_formatter_nivo_slider_themes();	
	

    $nivo_slider_effect = array(
      'sliceDown',
      'sliceDownLeft',
      'sliceUp',
      'sliceUpLeft',
      'sliceUpDown',
      'sliceUpDownLeft',
      'fold',
      'fade',
      'random',
      'slideInRight',
      'slideInLeft',
      'boxRandom',
      'boxRain',
      'boxRainReverse',
      'boxRainGrow',
      'boxRainGrowReverse'
    );
    $elements['nivo_formatter_theme'] = array(
      '#type' => 'select',
      '#title' => t('Nivo slider theme'),
      '#options' => $themes,
      '#default_value' => $settings['nivo_formatter_theme'],
      '#description' => t('Select your nivo slider theme.'),
    );
    $elements['nivo_formatter_image_style'] = array(
      '#type' => 'select',
      '#title' => t('Select image style'),
      '#options' => $image_styles,
      '#default_value' => $settings['nivo_formatter_image_style'],
      '#description' => t('Select the image style you would like to show.'),
    );
    $nivo_slider_thumbnail = $this->getSetting('nivo_slider_thumbnail');
    $elements['nivo_slider_thumbnail'] = array(
      '#type' => 'details',
      '#title' => t('Nivo Slider thumbnail settings'),
      '#open' => False,
    );
    $elements['nivo_slider_thumbnail']['controlNavThumbs'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable navigation thumbnail'),
      '#default_value' => $nivo_slider_thumbnail['controlNavThumbs'],
      '#description' => t('Using Thumbnails with the Nivo Slider'),
    );
    $elements['nivo_slider_thumbnail']['thumbnail_style'] = array(
      '#type' => 'select',
      '#title' => t('Select thumbnail image style'),
      '#options' => $image_styles,
      '#default_value' => $nivo_slider_thumbnail['thumbnail_style'],
      '#description' => t('Select the image style you would like thumbnail to show.'),
    );
    $nivo_formatter_nivo_slider = $this->getSetting('nivo_formatter_nivo_slider');
    $elements['nivo_formatter_nivo_slider'] = array(
      '#type' => 'details',
      '#title' => t('Nivo Slider advanced settings'),
      '#open' => False,
    );
    $elements['nivo_formatter_nivo_slider']['effect'] = array(
      '#type' => 'select',
      '#title' => t('effect'),
      '#options' => array_combine($nivo_slider_effect, $nivo_slider_effect),
      '#default_value' => $nivo_formatter_nivo_slider['effect'],
      '#description' => t('Specify sets like: \'fold,fade,sliceDown\''),
      '#multiple' => TRUE,
    );
    $elements['nivo_formatter_nivo_slider']['slices'] = array(
      '#type' => 'textfield',
      '#title' => t('slices'),
      '#default_value' => $nivo_formatter_nivo_slider['slices'],
      '#description' => t('For slice animations.'),
    );
    $elements['nivo_formatter_nivo_slider']['boxCols'] = array(
      '#type' => 'textfield',
      '#title' => t('boxCols'),
      '#default_value' => $nivo_formatter_nivo_slider['boxCols'],
      '#description' => t('For box animations.'),
    );
    $elements['nivo_formatter_nivo_slider']['boxRows'] = array(
      '#type' => 'textfield',
      '#title' => t('boxRows'),
      '#default_value' => $nivo_formatter_nivo_slider['boxRows'],
      '#description' => t('For box animations.'),
    );
    $elements['nivo_formatter_nivo_slider']['animSpeed'] = array(
      '#type' => 'textfield',
      '#title' => t('animSpeed'),
      '#default_value' => $nivo_formatter_nivo_slider['animSpeed'],
      '#description' => t('Slide transition speed.'),
    );
    $elements['nivo_formatter_nivo_slider']['pauseTime'] = array(
      '#type' => 'textfield',
      '#title' => t('pauseTime'),
      '#default_value' => $nivo_formatter_nivo_slider['pauseTime'],
      '#description' => t('How long each slide will show.'),
    );
    $elements['nivo_formatter_nivo_slider']['startSlide'] = array(
      '#type' => 'textfield',
      '#title' => t('startSlide'),
      '#default_value' => $nivo_formatter_nivo_slider['startSlide'],
      '#description' => t('Set starting Slide (0 index).'),
    );
    $elements['nivo_formatter_nivo_slider']['directionNav'] = array(
      '#type' => 'textfield',
      '#title' => t('directionNav'),
      '#default_value' => $nivo_formatter_nivo_slider['directionNav'],
      '#description' => t('Next & Prev navigation.'),
    );
    $elements['nivo_formatter_nivo_slider']['directionNavHide'] = array(
      '#type' => 'checkbox',
      '#title' => t('directionNavHide'),
      '#default_value' => $nivo_formatter_nivo_slider['directionNavHide'],
      '#description' => t('Only show on hover.'),
    );
    $elements['nivo_formatter_nivo_slider']['controlNav'] = array(
      '#type' => 'checkbox',
      '#title' => t('controlNav'),
      '#default_value' => $nivo_formatter_nivo_slider['controlNav'],
      '#description' => t('1,2,3... navigation.'),
    );
    $elements['nivo_formatter_nivo_slider']['keyboardNav'] = array(
      '#type' => 'checkbox',
      '#title' => t('keyboardNav'),
      '#default_value' => $nivo_formatter_nivo_slider['keyboardNav'],
      '#description' => t('Use left & right arrows.'),
    );
    $elements['nivo_formatter_nivo_slider']['pauseOnHover'] = array(
      '#type' => 'checkbox',
      '#title' => t('pauseOnHover'),
      '#default_value' => $nivo_formatter_nivo_slider['pauseOnHover'],
      '#description' => t('Stop animation while hovering.'),
    );
    $elements['nivo_formatter_nivo_slider']['manualAdvance'] = array(
      '#type' => 'checkbox',
      '#title' => t('manualAdvance'),
      '#default_value' => $nivo_formatter_nivo_slider['manualAdvance'],
      '#description' => t('Stop animation while hovering.'),
    );
    $elements['nivo_formatter_nivo_slider']['captionOpacity'] = array(
      '#type' => 'textfield',
      '#title' => t('captionOpacity'),
      '#default_value' => $nivo_formatter_nivo_slider['captionOpacity'],
      '#description' => t('Universal caption opacity.'),
    );
    $elements['nivo_formatter_nivo_slider']['prevText'] = array(
      '#type' => 'textfield',
      '#title' => t('prevText'),
      '#default_value' => $nivo_formatter_nivo_slider['prevText'],
      '#description' => t('Prev directionNav text.'),
    );
    $elements['nivo_formatter_nivo_slider']['nextText'] = array(
      '#type' => 'textfield',
      '#title' => t('Next'),
      '#default_value' => $nivo_formatter_nivo_slider['nextText'],
      '#description' => t('Next directionNav text.'),
    );
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary = [];
    $summary[] = $this->t('Nivo Slider: @NivoSlider', array('@NivoSlider' => 'Nivo Slider configuration'));
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();
    $files = $this->getEntitiesToView($items, $langcode);

    $entity = $items->getEntity();
    $field_name = $this->fieldDefinition->getName();
    $entity_id = $entity->id();
    $settings = $this->getSettings();

    $theme = $settings['nivo_formatter_theme'];
    $style = $settings['nivo_formatter_image_style'];
    $thumb_style = $settings['nivo_slider_thumbnail']['thumbnail_style'];
    $isThumb = FALSE;

    $data = array();
    $image = array();
    $attributes = array();

    foreach ($files as $delta => $file) {
      $image_uri = $file->getFileUri();
      $url = Url::fromUri(file_create_url($image_uri));
      $data[] = $url;
      if ($settings['nivo_slider_thumbnail']['controlNavThumbs']) {
        $isThumb = TRUE;
        // Check thumb style
        if (!empty($thumb_style)) {
          $image[$delta]['datathumb'] = ImageStyle::load($thumb_style)->buildUrl($image_uri);
        }
        else {
          $image[$delta]['datathumb'] = Url::fromUri(file_create_url($image_uri));
        }
      }
      if (!empty($style)) {
        $image[$delta]['url'] = ImageStyle::load($style)->buildUrl($image_uri);
      }
      else {
        $image[$delta]['url'] = Url::fromUri(file_create_url($image_uri));
      }
    }

    $elements = [
      '#attached' => array(
        'drupalSettings' => [
          'nivo_slider_image_formatter' => nivo_slider_image_formatter_settings("slider-" . $field_name . '-' . $entity_id, $settings),
        ],
        'library' => array(
          'nivo_slider_image_formatter/nivo_slider_image_formatter',
        ),
      ),
    ];
    $elements[0] = array(
      '#theme' => 'nivo_slider_image_formatter',
      '#images' => $image,
      '#settings' => $settings,
      '#nivo_slider_theme' => $theme,
      '#isThumb' => $isThumb,
      '#raws' => $attributes,
      '#field_name' => "slider-" . $field_name . '-' . $entity_id
    );
    return $elements;
  }

}
